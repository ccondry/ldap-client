# simple-ldap-client Change Log

Version numbers are semver-compatible dates in YYYY.MM.DD-X format, where
X is the revision number

# 2020.12.15

### Bug Fixes
* **Apply Changes:** Reject the promise with a "not found" error when
applyChanges finds no objects to apply the changes to. 


# 2020.10.24-1

### Features
* **Logging:** Reduce debug/trace logging to console


# 2020.10.24

### Features
* **Logging:** Reduce debug/trace logging to console


# 2020.10.23

### Features
* **Core:** add references to ldapjs library and changes array for client use
* **Remove From Group:** add removeFromGroup method
* **Logging:** Reduce debug/trace logging to console


# 2020.10.21

### Bug Fixes
* **Delete User:** Fix uncaught/unhandled exceptions thrown by client.del. They
will now be rejected from the promise.
